#ifndef BOARD_H_INCLUDED
#define BOARD_H_INCLUDED
#include <cstddef>
#include <fstream>
#include <string>
#include <vector>

class Board
{
public:
     Board();
     Board(size_t n_row, size_t n_col);
     ~Board();
     void Create();
     bool Create_from_file(std::string file);
     void Display(void);
     void Append(void);
     bool Set_Value(size_t row, size_t column, int value);
     int Get_Value(size_t row, size_t column);
     size_t Get_size_row(void);
     size_t Get_size_column(void);
     void Set_size_row(size_t row);
     void Set_size_column(size_t column);
private:
     size_t Search_Columns(std::string line);
     bool Validity_Value(size_t row, size_t column, int value);
     size_t m_row,m_column;
     std::vector<std::vector<int> > m_vecboard;
     std::ifstream file_csv;
};
#endif
