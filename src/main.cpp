#include "Board.h"
#include "Takuzu_Solve.h"
#include <cstddef>
#include <iterator>
#include <iostream>
#include <libintl.h>
#include <string>
#define _(String) gettext (String)

int main(int argc, char *argv[])
{
     Board tab;
     size_t lines(0),columns(0);
     bool back(true);
     setlocale (LC_ALL, "");
     std::string arg;
     arg=argv[1];
     if (argc==1)
     {
	  while ((lines<=0) || (lines%2!=0))
	  {
	       std::cout<<_("Give the number of lines of the Takazu board")<<std::endl;
	       std::cin>>lines;
	  }
	  while ((columns<=0) || (columns%2!=0))
	  {
	       std::cout<<_("Give the number of columns of the Takazu board")<<std::endl;
	       std::cin>>columns;
	  }
	  tab.Set_size_column(columns);
	  tab.Set_size_row(lines);
	  tab.Create();
	  tab.Append();
     }
     else if (argc==2)
     {
	  if (arg=="-h")
	  {
	       std::cout<<_("Takuzu, a program for solving a Takuzu grid")<<std::endl;
	       std::cout<<_("usage : Takuzu [OPTION]... [FILE]")<<std::endl;
	       std::cout<<_("-h        print this message")<<std::endl;
	       std::cout<<_("-f [FILE] solve the Takuzu grid. [FILE] must be a csv file")<<std::endl;
	       std::cout<<_("without option, create and solve a Takuzu grid")<<std::endl;
	  }
     }
     else if (argc==3)
     {
	  if (arg=="-f")
	  {
	       back=tab.Create_from_file(argv[2]);
	  }
     }
     if (back==true)
     {
	  tab.Display();
	  Takuzu_Solve takuzu(&tab);
	  takuzu.Solve();
     }
     else
     {
	  std::cout<<_("The .csv file is not valid");
     }
     return 0;
}
