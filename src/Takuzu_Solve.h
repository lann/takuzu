#ifndef TAKUZU_SOLVE_H_INCLUDED
#define TAKUZU_SOLVE_H_INCLUDED
#include "Board.h"
#include <cstddef>

class Takuzu_Solve
{
public:
     Takuzu_Solve();
     ~Takuzu_Solve();
     Takuzu_Solve(Board *board);
     void Solve(void);
private:
     int Find_double_line(size_t row, size_t column, int value);
     int Find_double_column(size_t row, size_t column, int value);
     bool Find_side_line(size_t row, size_t column, int value);
     bool Find_side_column(size_t row, size_t column, int value);
     bool Try_complete_line(size_t row, int value);
     bool Try_complete_column(size_t column, int value);
     Board *m_board;
     const int n_right_or_down_direction=1, n_left_or_up_direction=-1;
};
#endif
